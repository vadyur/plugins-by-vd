---
lang: ru-RU
title: Главная страница
description: Общее описание
heroImage: /images/logo.jpg
---
# Начало

Добро пожаловать! Здесь представлнены мои плагины, написанные для Коди.

[[toc]]

## Как установить

Все представленные плагины можно установить с помощью моего репозитория (скачать можно [здесь](/repo/repository.vd-2.0.0.zip)). Также плагины представлены в [repository.search.db](https://github.com/seppius-xbmc-repo/ru/tree/master/repository.search.db)

## Плагины

### Unwatched-TV Library

![Unwatched-TV Library разделы](/images/UnwtchdTV.png)

Данный плагин позволяет отслеживать для сериалов из медиатеки какие из новых сезонов вышли в мир. [Далее...](/vd-plugins/unwatched-tv.md)

### Lazy F1

![screen1](/images/LF1-1.png)

[Lazy F1](/vd-plugins/lazy-f1.md) - удобный способ просмотра Formula 1.

### TorrSpy

![TorrSpy](/images/torrspy.png)

Плагин предназначен для получения и отображении метаинформации о проигрываемом фильме/сериале запущенном в Kodi извне через TorrServer. Также данный аддон позволяет сохранить недосмотренный фильм/сериал в медиатеку Kodi. [Далее...](/vd-plugins/torrspy.md)

### TSC NEXT!

![TSC NEXT!](/images/tsc-next/icon.png)

Клиент ТоррСервер, основанный на плагине от авторов Nemiroff, virserg. Отличия от оригинала:
- Взаимодействие с TorrServer через модуль script.module.torrserver
- Проигрывание видео из медиатеки созданной TorrSpy, запоминание позиции проигрывания для медиатеки
- Реализует поиск раздач для плагина Unwatched-TV Library

### RuTracker

![RuTracker](/images/rutracker/icon.png)

Плагин автора HAL9000, переведён мной на Python 3, позволяет ему работать на KODI версии 19 и выше.