# Lazy F1
Lazy F1 - удобный способ просмотра Formula 1.

![screen1](/images/LF1-1.png)
![screen2](/images/LF1-2.png)
![screen3](/images/LF1-3.png)

Доступно:
- удобный каталог
- просмотр архива гонок
- запуск тв-трансляций