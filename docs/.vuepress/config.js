import { defaultTheme } from '@vuepress/theme-default'

export default {
    //base: process.env.NODE_ENV === 'development' ? '/' : '/plugins-by-vd/',
    dest: 'public',
    theme: defaultTheme({
        logo: '/images/logo.jpg',
        repo: 'https://github.com/vadyur',
        editLink: false,
        navbar: [
            // NavbarItem
            {
                text: 'Начало',
                link: '/',
            },
            // NavbarGroup
            {
                text: 'Плагины',
                children: [
                    '/vd-plugins/unwatched-tv.md',
                    '/vd-plugins/lazy-f1.md',
                    '/vd-plugins/torrspy.md'
                ],
            },
            // string - page file path
            {
                text: 'Обсудить',
                link: 'https://t.me/vd_plugins'
            },
        ],
    }),
}